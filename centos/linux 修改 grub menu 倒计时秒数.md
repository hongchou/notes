#1、编辑 /etc/default/grub 下的相应选项 
    vi /etc/default/grub
#2、使用 grub2-mkconfig 命令使之生效，-o 指 output，输出到真正有效力的 /boot/grub2/grub.cfg 文件 

    grub2-mkconfig -o /boot/grub2/grub.cfg
