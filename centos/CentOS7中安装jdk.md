# 利用yum源来安装jdk（此方法不需要配置环境变量） #
查看yum库中的java安装包 ：yum -y list java*

![](https://img-blog.csdn.net/20171208143606277?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMzI3ODY4NzM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

安装需要的jdk版本的所有java程序：yum -y install java-1.8.0-openjdk*

(安装完之后，默认的安装目录是在: /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.151-1.b12.el7_4.x86_64)

![](https://img-blog.csdn.net/20171208143844407?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMzI3ODY4NzM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

当结果出现了完毕！即安装完成。

查看java版本：java -version

![](https://img-blog.csdn.net/20171208144426251?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMzI3ODY4NzM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)