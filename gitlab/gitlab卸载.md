# 完全卸载GitLab #
## 一、停止gitlab ##
	sudo gitlab-ctl stop
## 二、卸载gitlab（这块注意了，看看是gitlab-ce版本还是gitlab-ee版本，别写错误了） ##
	sudo rpm -e gitlab-ce

## 三、查看gitlab进程 ##

	ps -ef|grep gitlab
## 杀掉第一个守护进程(runsvdir -P /opt/gitlab/service log) ##

    　    kill -9 4473
##再次查看gitlab进程是否存在 ##

## 四、删除gitlab文件 ##
##  删除所有包含gitlab的文件及目录 ##
    find / -name gitlab |xargs rm -rf 


## 删除gitlab-ctl uninstall时自动在root下备份的配置文件（ls /root/gitlab* 看看有没有，有也删除） ##

## 通过以上几步就可以彻底卸载gitlab ##