# centos7安装gitlab #
## 1. 安装配置依赖项 ##
如想使用Postfix来发送邮件,在安装期间请选择'Internet Site'. 您也可以用sendmai或者 配置SMTP服务 并 使用SMTP发送邮件.

在 Centos 6 和 7 系统上, 下面的命令将在系统防火墙里面开放HTTP和SSH端口.

    sudo yum install curl policycoreutils openssh-server openssh-clients
    sudo systemctl enable sshd
    sudo systemctl start sshd
    sudo yum install postfix
    sudo systemctl enable postfix
    sudo systemctl start postfix
    sudo firewall-cmd --permanent --add-service=http
    sudo systemctl reload firewalld
## 2. 添加GitLab仓库,并安装到服务器上 ##
    curl -sS http://packages.gitlab.cc/install/gitlab-ce/script.rpm.sh | sudo bash
    sudo yum install gitlab-ce
如果你不习惯使用命令管道的安装方式, 你可以在这里下载安装脚本或者手动下载您使用的系统相应的安装包(RPM/Deb) 然后安装，如下，使用清华大学gitlab的镜像（https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/），centos6使用e16目录下的，centos7使用e17目录下的

    curl -LJO https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-XXX.rpm
    rpm -i gitlab-ce-XXX.rpm
## 3. 启动GitLab ##

    sudo gitlab-ctl reconfigure

## 4. 使用浏览器访问GitLab ##
首次访问GitLab,系统会让你重新设置管理员的密码,设置成功后会返回登录界面.

默认的管理员账号是root,如果你想更改默认管理员账号,请输入上面设置的新密码登录系统后修改帐号名.

下边就可以访问了：

![](https://static.oschina.net/uploads/img/201702/10105046_sIe5.jpg)

重置下密码。登录效果如下：

![](https://static.oschina.net/uploads/img/201702/10105046_jVDM.jpg)

注意事项以及异常故障排查：

1、默认安装登录需要重置root密码。可以自己单独设置一个8位复杂密码后登录。

2、gitlab本身采用80端口，如安装前服务器有启用80，安装完访问会报错。需更改gitlab的默认端口。
修改vim /etc/gitlab/gitlab.rb：
external_url ‘http://localhost:90’

3、unicorn本身采用8080端口，如安装前服务器有启用8080，安装完访问会报错。需更改unicorn的默认端口。
修改 /etc/gitlab/gitlab.rb：
unicorn[‘listen’] = ‘127.0.0.1’
unicorn[‘port’] = 3000

4、每次重新配置，都需要执行sudo gitlab-ctl reconfigure  使之生效。

5、日志位置：/var/log/gitlab 可以进去查看访问日志以及报错日志等，供访问查看以及异常排查。
gitlab-ctl tail #查看所有日志
gitlab-ctl tail nginx/gitlab_access.log #查看nginx访问日志

6、如果入到安装后打不开，检查防火墙是否关闭。

7、安装版本和汉化版本一定要一致，如果不一致会导致502错误

(cat /opt/gitlab/embedded/service/gitlab-rails/VERSION   运行该命令进行gitlab版本检查)