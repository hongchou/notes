# Android 单选队列 RadioGroup与RadioButton详解 #
# 注：#  
RadioGroup继承至LinearLayout，所以LinearLayout的属性RadioGroup都可以使用。

# RadioButton特殊属性： #

	android:drawable 设置图片可以选着图片位置
	android:checked   控件是否选中
	android:button     隐藏圆圈

XML 代码：

	  <RadioGroup
        android:id="@+id/main_radiogroup"
        android:layout_width="match_parent"
        android:layout_height="@dimen/activity_main_radiogroup_height"
        android:background="@color/colorWhite"
        android:orientation="horizontal">
        <RadioButton
            android:id="@+id/main_radiobutton_weixing"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="1"
            android:textColor="@color/activity_main_radiobutton_text_color"
            android:textSize="@dimen/activity_main_radiobutton_text_size"
            android:text="@string/main_radiobutton_weixing"
            android:drawableTop="@drawable/radiobutton_main_weixing"
            android:gravity="center"
            android:checked="true"
            android:button="@null"
            android:background="@null"/>
        <RadioButton
            android:id="@+id/main_radiobutton_contacts"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="1"
            android:textColor="@color/activity_main_radiobutton_text_color"
            android:textSize="@dimen/activity_main_radiobutton_text_size"
            android:text="@string/main_radiobutton_contects"
            android:drawableTop="@drawable/radiobutton_main_contacts"
            android:gravity="center"
            android:button="@null"
            android:background="@null"/>
        <RadioButton
            android:id="@+id/main_radiobutton_find"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="1"
            android:textColor="@color/activity_main_radiobutton_text_color"
            android:textSize="@dimen/activity_main_radiobutton_text_size"
            android:text="@string/main_radiobutton_find"
            android:drawableTop="@drawable/radiobutton_main_find"
            android:gravity="center"
            android:button="@null"
            android:background="@null"/>
        <RadioButton
            android:id="@+id/main_radiobutton_my"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="1"
            android:textColor="@color/activity_main_radiobutton_text_color"
            android:textSize="@dimen/activity_main_radiobutton_text_size"
            android:text="@string/main_radiobutton_my"
            android:drawableTop="@drawable/radiobutton_main_my"
            android:gravity="center"
            android:button="@null"
            android:background="@null"/>
    </RadioGroup>

Activity监听代码：

	private void monitoringRadioGrop(){
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.main_radiobutton_weixing:
                        showToast(R.string.main_radiobutton_weixing);
                        break;
                    case R.id.main_radiobutton_contacts:
                        showToast(R.string.main_radiobutton_contects);
                        break;
                    case R.id.main_radiobutton_find:
                        showToast(R.string.main_radiobutton_find);
                        break;
                    case R.id.main_radiobutton_my:
                        showToast(R.string.main_radiobutton_my);
                        break;
                    default:
                        Log.d(TAG,"怎么监听的????");
                        break;
                }
            }
        });
    }

图片：radiobutton_main_weixing 实现，在res/drawable新建XML：

	<?xml version="1.0" encoding="utf-8"?>
	<selector xmlns:android="http://schemas.android.com/apk/res/android">

    <!-- 未选中-->
    <item android:drawable="@drawable/radiobutton_main_weixing_no" android:state_checked="false" />
    <!--选中-->
    <item android:drawable="@drawable/radiobutton_main_weixing_yes" android:state_checked="true" />
	</selector>

文本颜色修改：activity_main_radiobutton_text_color，在res下新建color文件夹在新建XML：

	<?xml version="1.0" encoding="utf-8"?>
	<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:state_checked="true"
    android:color="@color/button_app_main_yes_text"/>
    <!-- not selected -->
    <item android:state_checked="false"
        android:color="@color/button_app_main_no_text"/>
	</selector>
