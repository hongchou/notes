# android volley #
## 优点 ##
Volley可是说是把AsyncHttpClient和Universal-Image-Loader的优点集于了一身，

既可以像AsyncHttpClient一样非常简单地进行HTTP通信，

也可以像Universal-Image-Loader一样轻松加载网络上的图片。

除了简单易用之外，Volley在性能方面也进行了大幅度的调整，它的设计目标就是非常适合去进行数据量不大，但通信频繁的网络操作

## 缺点 ##
对于大数据量的网络操作，比如说下载文件等，Volley的表现就会非常糟糕。

## 使用步骤 ##
三步操作：

1. 创建一个RequestQueue对象。

2. 创建一个StringRequest对象。

3. 将StringRequest对象添加到RequestQueue里面。

## get请求示例 ##

	//获取到一个RequestQueue对象
	RequestQueue mQueue = Volley.newRequestQueue(context);
	
	//创建一个StringRequest对象
	StringRequest stringRequest = new StringRequest("http://www.baidu.com",
						new Response.Listener<String>() {
							@Override
							public void onResponse(String response) {
								Log.d("TAG", response);
							}
						}, new Response.ErrorListener() {
							@Override
							public void onErrorResponse(VolleyError error) {
								Log.e("TAG", error.getMessage(), error);
							}
						});
	
	//将这个StringRequest对象添加到RequestQueue里面就可以了
	mQueue.add(stringRequest);

## pos请求示例 ##

我们只需要在StringRequest的匿名类中重写getParams()方法，在这里设置POST参数就可以了

    StringRequest stringRequest = new StringRequest(Method.POST, url,  listener, errorListener) {
    	@Override
    	protected Map<String, String> getParams() throws AuthFailureError {
    		Map<String, String> map = new HashMap<String, String>();
    		map.put("params1", "value1");
    		map.put("params2", "value2");
    		return map;
    	}
    };

