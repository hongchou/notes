# Project build.gradle 添加： #

	 dependencies {
		//因为Android studio3.0的gredla与butterknife冲突，需要降级gredla!!!
        classpath 'com.android.tools.build:gradle:3.0.0'
	//        classpath 'com.android.tools.build:gradle:3.3.2'

        classpath 'com.jakewharton:butterknife-gradle-plugin:9.0.0'
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }

# 注意：这样编译时，还会报错，因为Android studio3.0的gredla与butterknife冲突，需要降级gredla!!! #

# 分支 module build.gradle 添加：(如果只有主项目可忽略) #

	apply plugin: 'com.jakewharton.butterknife'

	dependencies {
    annotationProcessor "com.jakewharton:butterknife-compiler:8.4.0"
	｝

# 分支 lib_common build.gradle 添加： #

	apply plugin: 'com.jakewharton.butterknife'

	dependencies {
    compile "com.jakewharton:butterknife:8.4.0"
	//如果不添加这一行，点击事件无效！！！前缀为annotationProcessor！！
    annotationProcessor "com.jakewharton:butterknife-compiler:8.4.0"
	｝

# 注意： annotationProcessor "com.jakewharton:butterknife-compiler:8.4.0"如果不添加这一行，点击事件无效！！！前缀为annotationProcessor！！ #
---

# ButterKnife Zelezny插件(自动生成代码) #

File→ Settings→ Plugins→Browse repositories→搜索Android ButterKnife Zelezny→Install→Restart Android Studio（安装后重启生效）

# ButterKnife 代码混淆： #

	-keep class butterknife.** { *; }  
	-dontwarn butterknife.internal.**  
	-keep class **$$ViewBinder { *; }  
	  
	-keepclasseswithmembernames class * {  
	    @butterknife.* <fields>;  
	}  
	  
	-keepclasseswithmembernames class * {  
	    @butterknife.* <methods>;  
	}

# 注意：这是编译项目还是会报错，要指定jdk版本 #

	.........
	com.android.tools.r8.CompilationFailedException: Compilation failed to complete	
	.........

# 解决办法 #

在module中指定jdk版本

	android {
    ..................

	//    指定java编译版本
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
	................
	}