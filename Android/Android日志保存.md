    https://blog.csdn.net/androidzhaoxiaogang/article/details/7948745

# 1、下载 #

到[http://code.google.com/p/microlog4android/downloads/list](http://code.google.com/p/microlog4android/downloads/list) 下载microlog4android-1.0.0.jar和microlog.properties文件（注意：

下载下来的microlog.properties是microlog.properties.txt，修改一下后缀名）

# 2、建立使用logger对象 #


    private static final Logger logger = LoggerFactory.getLogger(main.class);

# 3、在程序的第activit或者service的oncreate方法里初始化方法 #


    PropertyConfigurator.getConfigurator(this).configure();

# 4、把microlog.properties文件放到assets文件夹里 #

## 注意：assets文件夹是与res文件夹平级的 ##

然后更改microlog.properties文件为以下内容：

    microlog.level=DEBUG
    microlog.appender=LogCatAppender;FileAppender
    microlog.formatter=PatternFormatter
    microlog.formatter.PatternFormatter.pattern=%c [%P] %m %T

# 5、写日志记录 #

    logger.debug("my debug");

# 6、在AndroidManifest.xml 添加写sd卡的权限 #


    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

启动程序，然后到SD卡根目录，可以发现有一个microlog.txt的文件，里面就是我们记录的日志了。

# 补充说明（实现循环滚动的日志，也就是让日志不断的追加在上一条后面） #
1.上面的日志在多地方调用，有时候可能会发现只保存了一条记录，每次后面的日志都覆盖了前面的日志，所以我们需要增加点额外代码；

2.在第三步后面： final FileAppender  fa =  logger.getAppender(1);    fa.setAppend(true);  ok, 到此大功告成。