# Android设置点击物理返回键后应用后台运行而不是退出 #
## 方法一： ##
    //后台运行而不退出程序

	@Override
    public void onBackPressed() {//重写的Activity返回

        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        startActivity(intent);

	}

方法二：
首先在配置清单里添加启动模式

    android:launchMode="singleInstance"

放在AndroidManifest所不需要销毁的activity里添加

	   <activity android:name=".oidbluetooth.OidActivity"
            android:launchMode="singleInstance"
            ></activity>
            
然后在该activity添加一下代码，对返回键的操作，这样就轻松避免了activity被销毁

	 @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

# 注意：实测方法二，直接添加代码，对返回键操作即可 #