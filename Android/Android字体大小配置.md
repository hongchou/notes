# Android字体大小配置 #
在工程的res\values\目录下创建一个dimens.xml尺寸资源文件。

**其中wrap_content写到dimens中的值为-2dp，fill_parent或match_parent的值为-1dp.**
	
	<dimen name="my_wrap">-2dp</dimen>
	<dimen name="my_fill">-1dp</dimen>
	