#解决方法

##如果是想要禁用掉上述快键键，用以下命令就可以了：

    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['']"
    
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['']"
    
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up"['']"
    
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['']"

 

#注：

##我通常还是会使用WorkSpace，所以我习惯让IDE保留默认快捷键，而切换WorkSpace改用以下快捷键：

gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['<Super><Alt>Right']"

gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left  "['<Super><Alt>Left']"

gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up   "['<Super><Alt>Up']"

gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down"['<Super><Alt>Down']