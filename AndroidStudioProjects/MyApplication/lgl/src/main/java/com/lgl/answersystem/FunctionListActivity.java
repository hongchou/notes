package com.lgl.answersystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class FunctionListActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * 功能列表页面
     */
    private Button dati;
    private Button bt_wechat;
    public static String TAG="psd";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_function_list);

        initView();
    }

    private void initView() {
        /**
         * 初始化控件
         */
        dati = (Button) findViewById(R.id.bt_dati);
        bt_wechat = (Button) findViewById(R.id.bt_wechat);

        dati.setOnClickListener(this);
        bt_wechat.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_dati:
//                进入答题系统
                Log.i(TAG,"点击答题系统按钮");
                Intent intent1=new Intent(this,VerificationActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.bt_wechat:
//                    进入聊天页面
                Log.i(TAG,"点击聊天按钮");
                Intent intent2=new Intent(this,WechatActivity.class);
                startActivity(intent2);
                finish();
                break;
        }
    }
}
