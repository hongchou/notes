package com.lgl.answersystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.channels.Channels;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView xuehao;
    private TextView mima;
    private Button login;

    /**
     * 学生进入答题系统的验证页面
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        initView();

        initData();
    }

    private void initData() {
        /**
         * 从服务器获取学生信息
         */
    }

    private void initView() {
        /**
         * 初始化控件
         */
//        学号
        xuehao = (TextView) findViewById(R.id.xuehao);
//        密码
        mima = (TextView) findViewById(R.id.mima);
//登陆
        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
//                进入答题系统
                Log.i(FunctionListActivity.TAG, "点击登陆按钮");
//                验证账号密码是否正确
                String xuehaoStr = xuehao.getText().toString();
                String mimaStr = mima.getText().toString();
                if (TextUtils.isEmpty(xuehaoStr) && TextUtils.isEmpty(mimaStr)) {
                    Toast.makeText(this, "学号和密码不能为空", Toast.LENGTH_SHORT).show();
                } else {
                    if (xuehaoStr.equals("123") && mimaStr.equals("456")) {
                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(this, "学号或密码不能正确", Toast.LENGTH_SHORT).show();

                    }

                }

                break;

        }

    }
}
