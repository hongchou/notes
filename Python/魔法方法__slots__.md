# 魔法方法 __slots__#
## Python允许在定义class的时候，定义一个特殊的__slots__变量，来限制该class实例能添加的属性： ##

    >>> class Person(object):
    __slots__ = ("name", "age")
    
    >>> P = Person()
    >>> P.name = "老王"
    >>> P.age = 20
    >>> P.score = 100
    Traceback (most recent call last):
      File "<pyshell#3>", line 1, in <module>
    AttributeError: Person instance has no attribute 'score'
    >>>

# 注意: #
# 使用__slots__要注意，__slots__定义的属性仅对当前类实例起作用，对继承的子类是不起作用的 #