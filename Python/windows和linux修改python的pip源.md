# windows和linux修改python的pip源 #

## window平台修改pip源 ##

找到系统盘下C:\C:\Users\用户名\AppData\Roaming，APPData可能是隐藏文件，需要将隐藏关闭；

查看在Roaming文件夹下有没有一个pip文件夹，如果没有创建一个；

进入pip文件夹，创建一个pip.ini文件；

使用记事本的方式打开pip.ini文件，写入：

    [global]
	timeout = 6000  
	index-url = https://mirrors.aliyun.com/pypi/simple/ 
	trusted-host = mirrors.aliyun.com   
         
# linux平台修改pip源 #
## 方法一：临时修改 ##

# 在pip后指定源

    pip install ipython -i http://mirrors.aliyun.com/pypi/simple/ 

## 方法二：永久修改 ##
# 找到~/.pip/pip.conf,如果不存在就创建，加入

    [global]
    timeout = 10 
    index-url = http://mirrors.aliyun.com/pypi/simple/ 
    [install]
    trusted-host = mirrors.aliyun.com
