# Dajango 数据库切换为MYSQL的问题：Error loading MySQLdb module.解决方案 #

学习django搭建个人博客时候，采用MySQL作为后台数据库，遇到如下问题

django.core.exceptions.ImproperlyConfigured: Error loading MySQLdb module.

Did you install mysqlclient?

## 解决方案： ##

 1. 安装pymsql
    
     	pip install pymysql

2. 安装完毕，打开_init_.py,添加代码：

	    import pymysql 
    	pymysql.install_as_MySQLdb()

