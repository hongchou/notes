# Django 国际化 #
## 一，开启国际化的支持，需要在settings.py文件中设置 ##

	MIDDLEWARE_CLASSES = (
	    ...
	    'django.middleware.locale.LocaleMiddleware',
	)
	 
	 
	LANGUAGE_CODE = 'en'
	TIME_ZONE = 'UTC'
	USE_I18N = True
	USE_L10N = True
	USE_TZ = True
	 
	LANGUAGES = (
    ('en', ('English')),
    ('zh-hans', ('中文简体')),
    ('zh-hant', ('中文繁體')),
	)
	 
	#翻译文件所在目录，需要手工创建
	LOCALE_PATHS = (
	    os.path.join(BASE_DIR, 'locale'),
	)
	 
	TEMPLATE_CONTEXT_PROCESSORS = (
	    ...
	    "django.core.context_processors.i18n",
	)

# 二，生成需要翻译的文件： #

    python manage.py makemessages -l zh_hans
    python manage.py makemessages -l zh_hant


# 三，手工翻译 locale 中的 django.po #

    此处省去500字
    ...
     
     
    #: .\tutorial\models.py:23
    msgid "created at"
    msgstr "创建于"
     
    #: .\tutorial\models.py:24
    msgid "updated at"
    msgstr "更新于"
     
    ...
    此处省去几百字
# 四，编译一下，这样翻译才会生效 #


    python manage.py compilemessages
如果翻译不生效，请检查你的语言包的文件夹是不是有 中划线，请用下划线代替它。

比如 zh-hans 改成 zh_hans （但是要注意 setttings.py 中要用 中划线，不要也改了，就这一句话，你可能会浪费几个小时或几天）