#1.准备工作。

##①安装uwsgi。推荐使用pip安装，命令行输入：


    pip install uwsgi
## ②安装nginx。直接用sudo apt-get 安装，如果安装不了，应该是软件源的位置，可以修改一下，清华的，豆瓣的，阿里云的都可以，命令行输入： ##


    sudo apt-get install nginx
## ③django项目要能够跑起来，在项目目录下运行 ##

    python manage.py runserver
要能够跑得通，如果不行肯定是有部分包没有安装好。

# 2.在django项目下的setting.py文件中进行修改，便于下一步部署： #

## ①将debug改为False。如下： ##


    Debug = False
## ②在allow_host中添加服务器的ip以及localhost,如: ##


    Allow_host=['192.160.23.1','localhost',]
##③移植静态文件配置，这个主要是将原项目的静态文件移植出来以便访问。如下，注意括号中第二个参数为移植后的目录，不要和之前的静态文件目录相同就好。


    STATIC_ROOT = os.path.join(BASE_DIR, "/var/www")
## ④进行静态文件移植，完成步骤 ③之后，在django项目目录下运行 ##


    python manage.py collectstatic
# 3.配置uwsgi。对于uwsgi的配置有很多种方式，私以为 xml配置方式最简单，这儿就只说最简单的。在项目目录下新建socket.xml文件（与manage.py同级）内容如下： #

#xml配置方式
---
	<uwsgi>
	    <socket>:8000</socket>
	    <chdir>/home/user/project/hello</chdir>
	    <module>hello.wsgi</module>
	    <processes>4</processes> <!-- 进程数 --> 
	    <daemonize>uwsgi.log</daemonize>
	</uwsgi>

-----------


#ini配置方式

	[uwsgi]
	socket = 127.0.0.1:8001
	master = true         //主进程
	vhost = true          //多站模式
	no-site = true        //多站模式时不设置入口模块和文件
	workers = 2           //子进程数
	reload-mercy = 10     
	vacuum = true         //退出、重启时清理文件
	max-requests = 1000   
	limit-as = 512
	buffer-size = 30000
	pidfile = /var/run/uwsgi8001.pid    //pid文件，用于下面的脚本启动、停止该进程
	daemonize = /website/uwsgi8001.log

# 4.配置nginx。在/etc/nginx/目录下，找到nginx.conf文件，用vim打开它，然后在http{}内部加入下面内容。 #

 	server{
            listen 80;
            server_name localhost;
            charset utf-8;
          
            access_log off;
            location /static {
                alias /var/www/; #要与之前django的setting.py中添加的static静态文件转移目录一致
            }
            location / {
                uwsgi_pass  127.0.0.1:8000; #端口与原来的uwsgi中设置的端口一致
                include     /etc/nginx/uwsgi_params;
            }
        }
## 注意！注意！注意！原来nginx.conf配置有两行必须注释掉。（这就是我一直搞不好的原因。。。） ##

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
# 5.启动项目，完成部署。进入到项目目录下，依次执行以下三个命令： #

	killall -9 uwsgi
	uwsgi -x socket.xml
	service nginx reload
#第一个是先杀死现有的uwsgi进程，第二句是用xml进行uwsgi配置，第三步是重启nginx。中间如果有端口占用就杀死被占用的进程；如果有权限问题，加上sudo就好。

