## 1、生成迁移文件：根据模型类生成sql语句 ##

    python manage.py makemigrations

## 2、执行迁移：执行sql语句生成数据表 ##
	
	python manage.py migrate

# 使用django的管理 #

## 创建一个管理员用户 ##

    python manage.py createsuperuser

## 编辑settings.py文件，设置编码、时区 ##

	LANGUAGE_CODE = 'zh-Hans'
	TIME_ZONE = 'Asia/Shanghai'

## 向admin注册booktest的模型 ##

	from django.contrib import admin
	from models import BookInfo
	admin.site.register(BookInfo)

# 自定义管理页面 #

Django提供了admin.ModelAdmin类

通过定义ModelAdmin的子类，来定义模型在Admin界面的显示方式

    class QuestionAdmin(admin.ModelAdmin):
    ...
	admin.site.register(Question, QuestionAdmin)

## 列表页属性 ##

list_display：显示字段，可以点击列头进行排序

	list_display = ['pk', 'btitle', 'bpub_date']






































































# django中创建外键on_delete解析 #

而django自从1.9版本之后，on_delete这个参数就必须设置了

    #models.py
	class Author(models.Model):
	    author = models.CharField(max_length=250)
	 
	class Books(models.Model): 
	    book = models.ForeignKey(Author,on_delete=models.CASCADE)

**CASCADE：删除作者信息一并删除作者名下的所有书的信息；**

**PROTECT：删除作者的信息时，采取保护机制，抛出错误：即不删除Books的内容；**

**SET_NULL：只有当null=True才将关联的内容置空；**

**SET_DEFAULT：设置为默认值；**

**SET( )：括号里可以是函数，设置为自己定义的东西；**

**DO_NOTHING：字面的意思，啥也不干，你删除你的干我毛线关系**


