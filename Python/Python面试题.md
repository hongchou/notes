# 4、字典如何删除键和合并两个字典 #

del和update方法

![](http://upload-images.jianshu.io/upload_images/5926208-bd9133b5eade706f?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 8、python2和python3的range（100）的区别 #

python2返回列表，python3返回迭代器，节约内存

# 9、一句话解释什么样的语言能够用装饰器? #

函数可以作为参数传递的语言，可以使用装饰器

# 11、简述面向对象中__new__和__init__区别 #
__init__是初始化方法，创建对象后，就立刻被默认调用了，可接收参数，如图
![](http://upload-images.jianshu.io/upload_images/5926208-71660310eb38254c?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

1、__new__至少要有一个参数cls，代表当前类，此参数在实例化时由Python解释器自动识别

2、__new__必须要有返回值，返回实例化出来的实例，这点在自己实现__new__时要特别注意，可以return父类（通过super(当前类名, cls)）__new__出来的实例，或者直接是object的__new__出来的实例

3、__init__有一个参数self，就是这个__new__返回的实例，__init__在__new__的基础上可以完成一些其它初始化的动作，__init__不需要返回值

4、如果__new__创建的是当前类的实例，会自动调用__init__函数，通过return语句里面调用的__new__函数的第一个参数是cls来保证是当前类实例，如果是其他类的类名，；那么实际创建返回的就是其他类的实例，其实就不会调用当前类的__init__函数，也不会调用其他类的__init__函数。
![](http://upload-images.jianshu.io/upload_images/5926208-46a908853a84b74d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 13、列表[1,2,3,4,5],请使用map()函数输出[1,4,9,16,25]，并使用列表推导式提取出大于10的数，最终输出[16,25]
#map（）函数第一个参数是fun，第二个参数是一般是list，第三个参数可以写list，也可以不写，根据需求 #
![](http://upload-images.jianshu.io/upload_images/5926208-e37d78b02a203e4b?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 14、python中生成随机整数、随机小数、0--1之间小数方法 #

随机整数：random.randint(a,b),生成区间内的整数

随机小数：习惯用numpy库，利用np.random.randn(5)生成5个随机小数

0-1随机小数：random.random(),括号中不传参
![](http://upload-images.jianshu.io/upload_images/5926208-507078b866759ed0?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 18、数据表student有id,name,score,city字段，其中name中的名字可有重复，需要消除重复行,请写sql语句 #

    select  distinct  name  from  student

# 20、python2和python3区别？列举5个 #

1、Python3 使用 print 必须要以小括号包裹打印内容，比如 print('hi')

Python2 既可以使用带小括号的方式，也可以使用一个空格来分隔打印内容，比如 print 'hi'

2、python2 range(1,10)返回列表，python3中返回迭代器，节约内存

3、python2中使用ascii编码，python3中使用utf-8编码

4、python2中unicode表示字符串序列，str表示字节序列

      python3中str表示字符串序列，byte表示字节序列

5、python2中为正常显示中文，引入coding声明，python3中不需要

6、python2中是raw_input()函数，python3中是input()函数

# 24、字典根据键从小到大排序 #

dict={"name":"zs","age":18,"city":"深圳","tel":"1362626627"}
![](http://upload-images.jianshu.io/upload_images/5926208-dec781f9f8c8e740?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 25、利用collections库的Counter方法统计字符串每个单词出现的次数"kjalfj;ldsjafl;hdsllfdhg;lahfbl;hl;ahlf;h" #
![](http://upload-images.jianshu.io/upload_images/5926208-9872f803f37cea56?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 29、正则re.complie作用 #

re.compile是将正则表达式编译成一个对象，加快速度，并重复使用

# 31、两个列表[1,5,7,9]和[2,2,6,8]合并为[1,2,2,3,6,7,8,9] #
extend可以将另一个集合中的元素逐一添加到列表中，区别于append整体添加
![](http://upload-images.jianshu.io/upload_images/5926208-59949fe49b8e0716?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 32、用python删除文件和用linux命令删除文件方法 #

python：os.remove(文件名)

linux:       rm  文件名

# 33、log日志中，我们需要用时间戳记录error,warning等的发生时间，请用datetime模块打印当前时间戳 “2018-04-01 11:38:54” #

顺便把星期的代码也贴上了
![](http://upload-images.jianshu.io/upload_images/5926208-c90a5da019495bcd?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 34、数据库优化查询方法 #

外键、索引、联合查询、选择特定字段等等

# 35、请列出你会的任意一种统计图（条形图、折线图等）绘制的开源库，第三方也行 #

pychart、matplotlib

# 38、简述Django的orm #

ORM，全拼Object-Relation Mapping，意为对象-关系映射

实现了数据模型与数据库的解耦，通过简单的配置就可以轻松更换数据库，而不需要修改代码只需要面向对象编程,orm操作本质上会根据对接的数据库引擎，翻译成对应的sql语句,所有使用Django开发的项目无需关心程序底层使用的是MySQL、Oracle、sqlite....，如果数据库迁移，只需要更换Django的数据库引擎即可
![](http://upload-images.jianshu.io/upload_images/5926208-dc95e88adc682585?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 48、提高python运行效率的方法 #

1、使用生成器，因为可以节约大量内存

2、循环代码优化，避免过多重复代码的执行

3、核心模块用Cython  PyPy等，提高效率

4、多进程、多线程、协程

5、多个if elif条件判断，可以把最有可能先发生的条件放到前面写，这样可以减少程序判断的次数，提高效率

# 53、写一个单列模式 #

因为创建对象时__new__方法执行，并且必须return 返回实例化出来的对象所cls.__instance是否存在，不存在的话就创建对象，存在的话就返回该对象，来保证只有一个实例对象存在（单列），打印ID，值一样，说明对象同一个
![](http://upload-images.jianshu.io/upload_images/5926208-8027435413aa9da4?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 57、分别从前端、后端、数据库阐述web项目的性能优化 #

该题目网上有很多方法，我不想截图网上的长串文字，看的头疼，按我自己的理解说几点

## 前端优化： ##

1、减少http请求、例如制作精灵图

2、html和CSS放在页面上部，javascript放在页面下面，因为js加载比HTML和Css加载慢，所以要优先加载html和css,以防页面显示不全，性能差，也影响用户体验差

## 后端优化： ##

1、缓存存储读写次数高，变化少的数据，比如网站首页的信息、商品的信息等。应用程序读取数据时，一般是先从缓存中读取，如果读取不到或数据已失效，再访问磁盘数据库，并将数据再次写入缓存。

2、异步方式，如果有耗时操作，可以采用异步，比如celery

3、代码优化，避免循环和判断次数太多，如果多个if else判断，优先判断最有可能先发生的情况

## 数据库优化： ##

1、如有条件，数据可以存放于redis，读取速度快

2、建立索引、外键等

# 59、列出常见MYSQL数据存储引擎 #

**InnoDB**：支持事务处理，支持外键，支持崩溃修复能力和并发控制。如果需要对事务的完整性要求比较高（比如银行），要求实现并发控制（比如售票），那选择InnoDB有很大的优势。如果需要频繁的更新、删除操作的数据库，也可以选择InnoDB，因为支持事务的提交（commit）和回滚（rollback）。 

**MyISAM**：插入数据快，空间和内存使用比较低。如果表主要是用于插入新记录和读出记录，那么选择MyISAM能实现处理高效率。如果应用的完整性、并发性要求比 较低，也可以使用。

**MEMORY**：所有的数据都在内存中，数据的处理速度快，但是安全性不高。如果需要很快的读写速度，对数据的安全性要求较低，可以选择MEMOEY。它对表的大小有要求，不能建立太大的表。所以，这类数据库只使用在相对较小的数据库表。

# 61、简述同源策略 #

 同源策略需要同时满足以下三点要求： 

1）协议相同 

 2）域名相同 

3）端口相同 

 http:www.test.com与https:www.test.com 不同源——协议不同 

 http:www.test.com与http:www.admin.com 不同源——域名不同 

 http:www.test.com与http:www.test.com:8081 不同源——端口不同

 只要不满足其中任意一个要求，就不符合同源策略，就会出现“跨域”

# 62、简述cookie和session的区别 #

1，session 在服务器端，cookie 在客户端（浏览器）

2、session 的运行依赖 session id，而 session id 是存在 cookie 中的，也就是说，如果浏览器禁用了 cookie ，同时 session 也会失效，存储Session时，键与Cookie中的sessionid相同，值是开发人员设置的键值对信息，进行了base64编码，过期时间由开发人员设置

3、cookie安全性比session差

# 66、python中copy和deepcopy区别 #
1、复制不可变数据类型，不管copy还是deepcopy,都是同一个地址当浅复制的值是不可变对象（数值，字符串，元组）时和=“赋值”的情况一样，对象的id值与浅复制原来的值相同。
![](http://upload-images.jianshu.io/upload_images/5926208-b270504aa82432ae?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

2、复制的值是可变对象（列表和字典）

浅拷贝copy有两种情况：

第一种情况：复制的 对象中无 复杂 子对象，原来值的改变并不会影响浅复制的值，同时浅复制的值改变也并不会影响原来的值。原来值的id值与浅复制原来的值不同。

第二种情况：复制的对象中有 复杂 子对象 （例如列表中的一个子元素是一个列表）， 改变原来的值 中的复杂子对象的值  ，会影响浅复制的值。

深拷贝deepcopy：完全复制独立，包括内层列表和字典
![](http://upload-images.jianshu.io/upload_images/5926208-3d23bac355b4642d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![](http://upload-images.jianshu.io/upload_images/5926208-93811031aeaa737f?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 69、请将[i for i in range(3)]改成生成器 #

生成器是特殊的迭代器，

1、列表表达式的[ ]改为（）即可变成生成器

2、函数在返回值得时候出现yield就变成生成器，而不是函数了；

中括号换成小括号即可，有没有惊呆了

![](http://upload-images.jianshu.io/upload_images/5926208-2a114f6019a5240a?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 71、举例sort和sorted对列表排序，list=[0,-1,3,-10,5,9] #
![](http://upload-images.jianshu.io/upload_images/5926208-b4ab4fe8a4e98a84?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 81、举例说明SQL注入和解决办法 #
当以字符串格式化书写方式的时候，如果用户输入的有;+SQL语句，后面的SQL语句会执行，比如例子中的SQL注入会删除数据库demo

![](http://upload-images.jianshu.io/upload_images/5926208-775192ddeb9fe36a?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

解决方式：通过传参数方式解决SQL注入

![](http://upload-images.jianshu.io/upload_images/5926208-3c2c8318190f2717?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 86、MyISAM 与 InnoDB 区别： #

1、InnoDB 支持事务，MyISAM 不支持，这一点是非常之重要。事务是一种高

级的处理方式，如在一些列增删改中只要哪个出错还可以回滚还原，而MyISAM就不可以了；

2、MyISAM 适合查询以及插入为主的应用，InnoDB 适合频繁修改以及涉及到安全性较高的应用；

3、InnoDB 支持外键，MyISAM 不支持；

4、对于自增长的字段，InnoDB 中必须包含只有该字段的索引，但是在 MyISAM表中可以和其他字段一起建立联合索引；

5、清空整个表时，InnoDB 是一行一行的删除，效率非常慢。MyISAM 则会重建表；

# 96、简述乐观锁和悲观锁 #

悲观锁, 就是很悲观，每次去拿数据的时候都认为别人会修改，所以每次在拿数据的时候都会上锁，这样别人想拿这个数据就会block直到它拿到锁。传统的关系型数据库里边就用到了很多这种锁机制，比如行锁，表锁等，读锁，写锁等，都是在做操作之前先上锁。

乐观锁，就是很乐观，每次去拿数据的时候都认为别人不会修改，所以不会上锁，但是在更新的时候会判断一下在此期间别人有没有去更新这个数据，可以使用版本号等机制，乐观锁适用于多读的应用类型，这样可以提高吞吐量

# 98、Linux命令重定向 > 和 >> #
Linux 允许将命令执行结果 重定向到一个 文件

将本应显示在终端上的内容 输出／追加 到指定文件中

    > 表示输出，会覆盖文件原有的内容
    
    >> 表示追加，会将内容追加到已有文件的末尾

# 100、python传参数是传值还是传址？ #
Python中函数参数是引用传递（注意不是值传递）。对于不可变类型（数值型、字符串、元组），因变量不能修改，所以运算不会影响到变量自身；而对于可变类型（列表字典）来说，函数体运算可能会更改传入的参数变量。
![](http://upload-images.jianshu.io/upload_images/5926208-91d503f0016d0808?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 101、求两个列表的交集、差集、并集 #
![](http://upload-images.jianshu.io/upload_images/5926208-340440e4758f96c2?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# 108、python中读取Excel文件的方法 #
应用数据分析库pandas
![](http://upload-images.jianshu.io/upload_images/5926208-673f01b5296445cf?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

