# 安装Python3 #

# 安装虚拟环境 #
    pip install virtualenv
    pip install virtualenvwrapper
## 一定是使用pip安装虚拟环境。不能使用pip3安装虚拟环境，不然没有virtualenvwrapper.sh文件 ##

# 3.配置环境变量，增加最后两行  #
    vim ~/.bashrc 
    export WORKON_HOME=/root/.virtualenvs 
    source /usr/local/python3.6.5/bin/virtualenvwrapper.sh 
## 根据实际情况配置环境变量 ##

# 运行测试： #

    source ~/.bashrc 

# 创建虚拟环境 #
## 在python3中，创建虚拟环境，需要联网！！！
## mkvirtualenv -p python3 虚拟环境名称 ##
## 例 ： ##

    mkvirtualenv -p python3 Django_virtualenv

## 使用虚拟环境的命令 ##
## 选择虚拟环境:  ##

    workon 两次tab键 

## 退出虚拟环境  ##

    deactivate

## 删除虚拟环境 ## 

    rmvirtualenv 虚拟环境名称 
## 例 ：删除虚拟环境py3_flask ##
## 先退出：deactivate  ##
## 再删除： ##

    rmvirtualenv py3_flask

