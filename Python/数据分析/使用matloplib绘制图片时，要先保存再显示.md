# 使用matloplib绘制图片时，要先保存再显示，不然保存的是空白图片 #

示例代码：

    from matplotlib import pyplot as plt
    
    x = range(2, 26, 2)
    y = [15, 13, 14.5, 17, 20, 25, 26, 26, 27, 22, 18, 15]
    
    # 设置图片大小
    fig = plt.figure(figsize=(20, 8), dpi=80)
    # 绘图
    plt.plot(x, y)
    
    plt.title("每2小时气温变化")
    
    plt.savefig('./t1.png')
    
    # 保存为矢量图，放大不会有锯齿
    plt.savefig('./t1.svg')
    
    # 显示
    plt.show()
    # 在这个地方保存，保存的图片是空白图片
    # plt.savefig('./t1.png')
