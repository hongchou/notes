matloplib中文显示

1、设置字体

拷贝字典到当前目录

	from matplotlib import font_manager
		...
    my_font_manager = font_manager.FontProperties(fname='./simsun.ttc')

2、在要显示中文的地方设置fontproperties
	

    plt.title("每2小时气温变化",fontproperties=my_font_manager)
