# 1.首先，你要知道系统现在的python的位置在哪儿： #

    [root@root ~]# whereis python
    python: /usr/bin/python2.7 /usr/bin/python /usr/lib/python2.7 /usr/lib64/python2.7 /etc/python /usr/include/python2.7 /usr/share/man/man1/python.1.gz

# 可以知道我们的python在 /usr/bin目录中 #

	[root@root ~]# cd /usr/bin/
	[root@root bin]# ll python*
	lrwxrwxrwx. 1 root root7 2月   7 09:30 python -> python2
	lrwxrwxrwx. 1 root root9 2月   7 09:30 python2 -> python2.7
	-rwxr-xr-x. 1 root root 7136 8月   4 2017 python2.7

可以看到，python指向的是python2，python2指向的是python2.7，因此我们可以装个python3，然后将python指向python3，然后python2指向python2.7，那么两个版本的python就能共存了。

# 2.因为我们要安装python3，所以要先安装相关包，用于下载编译python3： #

    yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gcc make 
运行了以上命令以后，就安装了编译python3所用到的相关依赖

# 3.默认的，centos7也没有安装pip，不知道是不是因为我安装软件的时候选择的是最小安装的模式。 #
#运行这个命令添加epel扩展源

    yum -y install epel-release

#安装pip

    yum install python-pip

# 4.用pip装wget #

    pip install wget


# 5.用wget下载python3的源码包 #

    wget https://www.python.org/ftp/python/3.6.4/Python-3.6.4.tar.xz

# 6.编译python3源码包 #
#解压
    xz -d Python-3.6.4.tar.xz
    tar -xf Python-3.6.4.tar

#进入解压后的目录，依次执行下面命令进行手动编译
    ./configure prefix=/usr/local/python3
    make && make install

如果最后没提示出错，就代表正确安装了，在/usr/local/目录下就会有python3目录

# 7.添加软链接 #
#将原来的链接备份

    mv /usr/bin/python /usr/bin/python.bak

#添加python3的软链接

    ln -s /usr/local/python3/bin/python3.6 /usr/bin/python3

#测试是否安装成功了

    python3 -V