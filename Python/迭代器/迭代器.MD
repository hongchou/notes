# 迭代器（Iterator） #
## 2. 判断是否可以迭代 ##
## 可以使用 isinstance() 判断一个对象是否是 Iterable 对象： ##
	In [50]: from collections import Iterable

	In [51]: isinstance([], Iterable)
	Out[51]: True
	
	In [52]: isinstance({}, Iterable)
	Out[52]: True
	
	In [53]: isinstance('abc', Iterable)
	Out[53]: True
	
	In [54]: isinstance((x for x in range(10)), Iterable)
	Out[54]: True
	
	In [55]: isinstance(100, Iterable)
	Out[55]: False

# 可以被next()函数调用并不断返回下一个值的对象称为迭代器：Iterator。 #

# iter()函数 #
## 生成器都是Iterator对象，但list、dict、str虽然是Iterable却不是Iterator。 ##
## 把 list 、 dict 、 str 等 Iterable 变成 Iterator 可以使用 iter() 函数： ##

	In [62]: isinstance(iter([]), Iterator)
	Out[62]: True
	
	In [63]: isinstance(iter('abc'), Iterator)
	Out[63]: True
# 总结 #
凡是可作用于 for 循环的对象都是 Iterable 类型；

凡是可作用于 next() 函数的对象都是 Iterator 类型

集合数据类型如 list 、 dict 、 str 等是 Iterable 但不是 Iterator ，不过可以通过 iter() 函数获得一个 Iterator 对象。