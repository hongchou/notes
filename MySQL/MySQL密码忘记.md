#强制进行修改密码
    
    systemctl stop mysqld
    
    systemctl set-environment MYSQLD_OPTS="--skip-grant-tables"
    
    systemctl start mysqld
    
    mysql -u root
    
    UPDATE mysql.user SET authentication_string = PASSWORD('MyNewPassword') WHERE User = 'root' AND Host = 'localhost';

##注：如果显示 ERROR 1819 (HY000): Your password does not satisfy the current policy requirements，说明你的密码不符合安全要求，
    
    FLUSH PRIVILEGES;

    quit;