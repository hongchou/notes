    CREATE DATABASE IF NOT EXISTS dbname DEFAULT CHARSET utf8mb4;

#为什么使用utf8mb4字符集
存储表情符号的时候使用，不然会是乱码

utf8最大的一个特点，就是它是一种变长的编码方式。它可以使用1~4个字节表示一个符号，根据不同的符号而变化字节长度。其中Emoji表情是4个字节，而MySql的utf8编码最多3个字节，所以导致了数据插不进去。

准备：
mysql从5.5.3开始支持utf8mb4

#3、对于我们自己设置的数据库
3.1、修改mysql的配置文件/etc/mysql/my.cnf, 添加如下内容
修改mysql的配置文件/etc/mysql/my.cnf, 添加如下内容
    [client]
    default-character-set=utf8mb4
    
    [mysql]
    default-character-set=utf8mb4
    
    [mysqld]
    character-set-client-handshake=FALSE
    character-set-server=utf8mb4
    collation-server=utf8mb4_unicode_ci
    init_connect='SET NAMES utf8mb4'

3.2、重启数据库

    mysql restart
3.3、检查配置

    mysql> SHOW VARIABLES WHERE Variable_name LIKE 'char%' OR Variable_name LIKE 'collation%';

+--------------------------+--------------------+
| Variable_name            | Value              |
+--------------------------+--------------------+
| character_set_client     | utf8mb4            |
| character_set_connection | utf8mb4            |
| character_set_database   | utf8mb4            |
| character_set_filesystem | binary             |
| character_set_results    | utf8mb4            |
| character_set_server     | utf8mb4            |
| character_set_system     | utf8               |
| collation_connection     | utf8mb4_unicode_ci |
| collation_database       | utf8mb4_unicode_ci |
| collation_server         | utf8mb4_unicode_ci |
+--------------------------+--------------------+
11 rows in set (0.05 sec)

mysql>

collation_connection/collation_database/collation_server如果是utf8mb4_general_ci没有关系
但必须保证:  character_set_client/character_se_connection/character_set_database/character_set_results/character_set_server为utf8mb4。


#4、建库或者是更新编码
1、创建数据库

    create database duodian default character set utf8mb4 collate utf8mb4_unicode_ci ;

2、修改数据库

    ALTER DATABASE duodianyouhui CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ;

3、修改表

    ALTER TABLE user_info CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;

4、修改字段

    alter table user_info modify column nickName varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci