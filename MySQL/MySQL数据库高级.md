# MySQL索引

添加索引的命令

`create index 索引名称	on 表名（字段名（字段长度））;`

## 开启MySQL运行时间及查看运行时间的命令

开启运行时间监测

set	profiling=1；

查看执行的时间

show profiles;

# 索引的目的：提高查询效率

## 创建账户&授权

grant 权限列表 on 数据库 to ‘用户名’@‘访问主机’ identified by ‘密码’;

## 创建用户&并给与所有权限

grant  all  privileges on 数据库 to ‘用户名’@‘访问主机’ identified by ‘密码’;

## 查看用户有哪些权限

show  grant  for   ‘用户名’@‘访问主机’;

## 修改权限

grant 权限列表 on 数据库 to ‘用户名’@‘访问主机’  with  grant  option;

## 修改MySQL密码

update  user  set  authentication_dtring=password('新密码')  where user=‘用户名’;

**每次修改权限或者密码后，要刷新才有效！！！**

flush  privileges;

# 主从数据库配置

1. 实时灾备，用于故障切换
1. 读写分离，提供查询服务
1. 备份，避免影响业务

    https://blog.csdn.net/envon123/article/details/76615059